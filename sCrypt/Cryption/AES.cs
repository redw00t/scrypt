﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace sCrypt.Cryption
{
    public class AES
    {
        public static string Rijndaelcrypt(string File, string Key)
        {
            RijndaelManaged oAesProvider = new RijndaelManaged();
            byte[] btClear = null;
            byte[] btSalt = new byte[] {
                        1,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7,
                        8,
                };
            Rfc2898DeriveBytes oKeyGenerator = new Rfc2898DeriveBytes(Key, btSalt);
            oAesProvider.Key = oKeyGenerator.GetBytes(oAesProvider.Key.Length);
            oAesProvider.IV = oKeyGenerator.GetBytes(oAesProvider.IV.Length);
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            CryptoStream cs = new CryptoStream(ms, oAesProvider.CreateEncryptor(), CryptoStreamMode.Write);
            btClear = System.Text.Encoding.UTF8.GetBytes(File);
            cs.Write(btClear, 0, btClear.Length);
            cs.Close();
            File = Convert.ToBase64String(ms.ToArray());
            return (string)(File);
        }
    }
}
