﻿using System.CodeDom.Compiler;
using Microsoft.CSharp;
using System.Collections.Generic;
using System;
using System.Windows.Forms;

namespace sCrypt
{
    class Compiler
    {
        public static bool Compile(string output, string source, string icon, string resource)
        {
            CompilerParameters para = new CompilerParameters();
            CompilerResults cres = default(CompilerResults);

            Dictionary<string, string> provoptions = new Dictionary<string, string>();
            provoptions.Add("CompilerVersion", "v4.0");

            CSharpCodeProvider Compiler = new CSharpCodeProvider(provoptions);
            para.GenerateExecutable = true;
            para.TreatWarningsAsErrors = false;
            para.OutputAssembly = output;
            para.EmbeddedResources.Add(resource);
            para.ReferencedAssemblies.Add("System.dll");
            para.ReferencedAssemblies.Add("System.Security.dll");
            para.ReferencedAssemblies.Add("System.Windows.Forms.dll");
            para.ReferencedAssemblies.Add("System.IO.Compression.dll");
            para.ReferencedAssemblies.Add("System.IO.Compression.FileSystem.dll");
            para.ReferencedAssemblies.Add("System.Xml.dll");
            para.ReferencedAssemblies.Add("mscorlib.dll");
            para.CompilerOptions = "/platform:x86";

            if (!string.IsNullOrEmpty(icon))
            {
                para.CompilerOptions += " /win32icon:" + icon;
            }

            cres = Compiler.CompileAssemblyFromSource(para, source);

            if (cres.Errors.Count > 0)
            {
                foreach (CompilerError errr in cres.Errors)
                {
                    MessageBox.Show(errr.Line + "\n" + errr.ErrorText);
                }
                return false;
            }
            return true;
        }
    }
}
