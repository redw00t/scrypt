﻿namespace sCrypt
{
    partial class frmMain
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lb_items = new System.Windows.Forms.ListBox();
            this.txt_pwd1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_pwd = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cb_encryption = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_browse = new System.Windows.Forms.Button();
            this.txt_path = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lb_status = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cb_keyfile = new System.Windows.Forms.CheckBox();
            this.btn_encrypt = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsb_exit = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsb_help = new System.Windows.Forms.ToolStripMenuItem();
            this.tsb_update = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lb_items);
            this.groupBox1.Controls.Add(this.txt_pwd1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txt_pwd);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cb_encryption);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btn_browse);
            this.groupBox1.Controls.Add(this.txt_path);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(284, 256);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "General Settings";
            // 
            // lb_items
            // 
            this.lb_items.AllowDrop = true;
            this.lb_items.FormattingEnabled = true;
            this.lb_items.Location = new System.Drawing.Point(9, 58);
            this.lb_items.Name = "lb_items";
            this.lb_items.Size = new System.Drawing.Size(269, 69);
            this.lb_items.TabIndex = 100;
            this.lb_items.DragDrop += new System.Windows.Forms.DragEventHandler(this.lb_items_DragDrop);
            this.lb_items.DragEnter += new System.Windows.Forms.DragEventHandler(this.lb_items_DragEnter);
            // 
            // txt_pwd1
            // 
            this.txt_pwd1.Location = new System.Drawing.Point(9, 225);
            this.txt_pwd1.Name = "txt_pwd1";
            this.txt_pwd1.PasswordChar = '•';
            this.txt_pwd1.Size = new System.Drawing.Size(269, 20);
            this.txt_pwd1.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 209);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(194, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Please repeat the encryption Password:";
            // 
            // txt_pwd
            // 
            this.txt_pwd.Location = new System.Drawing.Point(9, 186);
            this.txt_pwd.Name = "txt_pwd";
            this.txt_pwd.PasswordChar = '•';
            this.txt_pwd.Size = new System.Drawing.Size(269, 20);
            this.txt_pwd.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 170);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(175, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Please set an encryption Password:";
            // 
            // cb_encryption
            // 
            this.cb_encryption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_encryption.FormattingEnabled = true;
            this.cb_encryption.Items.AddRange(new object[] {
            "Advanced Encryption Standard (AES) 256 Bit",
            "Blowfish",
            "AES 256 Bit - Blowfish"});
            this.cb_encryption.Location = new System.Drawing.Point(9, 146);
            this.cb_encryption.Name = "cb_encryption";
            this.cb_encryption.Size = new System.Drawing.Size(269, 21);
            this.cb_encryption.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(178, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Please select an encryption method:";
            // 
            // btn_browse
            // 
            this.btn_browse.Location = new System.Drawing.Point(251, 32);
            this.btn_browse.Name = "btn_browse";
            this.btn_browse.Size = new System.Drawing.Size(27, 20);
            this.btn_browse.TabIndex = 1;
            this.btn_browse.Text = "...";
            this.btn_browse.UseVisualStyleBackColor = true;
            this.btn_browse.Click += new System.EventHandler(this.btn_browse_Click);
            // 
            // txt_path
            // 
            this.txt_path.Location = new System.Drawing.Point(9, 32);
            this.txt_path.Name = "txt_path";
            this.txt_path.ReadOnly = true;
            this.txt_path.Size = new System.Drawing.Size(236, 20);
            this.txt_path.TabIndex = 10;
            this.txt_path.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 13);
            this.label1.TabIndex = 99;
            this.label1.Text = "Please choose a file to crypt:";
            // 
            // lb_status
            // 
            this.lb_status.FormattingEnabled = true;
            this.lb_status.Location = new System.Drawing.Point(9, 19);
            this.lb_status.Name = "lb_status";
            this.lb_status.Size = new System.Drawing.Size(269, 56);
            this.lb_status.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cb_keyfile);
            this.groupBox2.Controls.Add(this.btn_encrypt);
            this.groupBox2.Controls.Add(this.lb_status);
            this.groupBox2.Location = new System.Drawing.Point(12, 289);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(284, 113);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Build";
            // 
            // cb_keyfile
            // 
            this.cb_keyfile.AutoSize = true;
            this.cb_keyfile.Location = new System.Drawing.Point(9, 85);
            this.cb_keyfile.Name = "cb_keyfile";
            this.cb_keyfile.Size = new System.Drawing.Size(103, 17);
            this.cb_keyfile.TabIndex = 6;
            this.cb_keyfile.Text = "Generate keyfile";
            this.cb_keyfile.UseVisualStyleBackColor = true;
            this.cb_keyfile.CheckedChanged += new System.EventHandler(this.cb_keyfile_CheckedChanged);
            // 
            // btn_encrypt
            // 
            this.btn_encrypt.Location = new System.Drawing.Point(203, 81);
            this.btn_encrypt.Name = "btn_encrypt";
            this.btn_encrypt.Size = new System.Drawing.Size(75, 23);
            this.btn_encrypt.TabIndex = 5;
            this.btn_encrypt.Text = "Encrypt File";
            this.btn_encrypt.UseVisualStyleBackColor = true;
            this.btn_encrypt.Click += new System.EventHandler(this.btn_encrypt_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(308, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsb_exit});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // tsb_exit
            // 
            this.tsb_exit.Name = "tsb_exit";
            this.tsb_exit.Size = new System.Drawing.Size(92, 22);
            this.tsb_exit.Text = "Exit";
            this.tsb_exit.Click += new System.EventHandler(this.tsb_exit_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsb_help,
            this.tsb_update});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // tsb_help
            // 
            this.tsb_help.Name = "tsb_help";
            this.tsb_help.Size = new System.Drawing.Size(172, 22);
            this.tsb_help.Text = "Help";
            // 
            // tsb_update
            // 
            this.tsb_update.Name = "tsb_update";
            this.tsb_update.Size = new System.Drawing.Size(172, 22);
            this.tsb_update.Text = "Search for Updates";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(308, 411);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.Text = "sCrypt | simpleCrypt";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txt_pwd1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_pwd;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cb_encryption;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_browse;
        private System.Windows.Forms.TextBox txt_path;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lb_status;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_encrypt;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsb_exit;
        private System.Windows.Forms.ToolStripMenuItem tsb_help;
        private System.Windows.Forms.ToolStripMenuItem tsb_update;
        private System.Windows.Forms.ListBox lb_items;
        private System.Windows.Forms.CheckBox cb_keyfile;
    }
}

