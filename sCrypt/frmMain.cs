﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Xml.Linq;

namespace sCrypt
{
    public partial class frmMain : Form
    {
        string appdata = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        List<string> paths = new List<string>();

        const string version = "0.0.5.0";

        public frmMain()
        {
            InitializeComponent();
            onClose();
            System.IO.Directory.CreateDirectory(appdata + @"\sCrypt");
        }

        private void btn_browse_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = true;
            ofd.Filter = "All files (*.*)|*.*";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                foreach (string fn in ofd.FileNames)
                {
                    System.IO.File.Copy(fn, appdata + @"\sCrypt\" + Path.GetFileName(fn));
                    paths.Add(appdata + @"\sCrypt" + Path.GetFileName(fn));
                    txt_path.Text = appdata + @"\sCrypt" + Path.GetFileName(fn);
                    lb_items.Items.Add(Path.GetFileName(fn));
                }
            }
        }

        private void btn_encrypt_Click(object sender, EventArgs e)
        {
            System.IO.Compression.ZipFile.CreateFromDirectory(appdata + @"\sCrypt", appdata + @"\data.zip");
            if (txt_pwd.Text == txt_pwd1.Text)
            {
                if (cb_keyfile.Checked == true)
                {
                    SaveFileDialog sfd = new SaveFileDialog();
                    sfd.Filter = "sCrypt Keyfile (*.scryptk)|*.scryptk";
                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        var doc = new XDocument(new XElement("sCrypt", new XAttribute("Version", version),
                        new XElement("key", Cryption.AES.Rijndaelcrypt(txt_pwd.Text, Cryption.MD5.getMD5(txt_pwd.Text)))));
                        System.IO.File.WriteAllText(sfd.FileName, doc.ToString());
                    }
                }
                string bytes = null;
                UpdateStatus("Reading bytes..."); bytes = Convert.ToBase64String(System.IO.File.ReadAllBytes(appdata + @"\data.zip")); UpdateStatus("Successful!");
                if (cb_encryption.Text == "Advanced Encryption Standard (AES) 256 Bit")
                {
                    UpdateStatus("Encrypting bytes... (AES) 256 Bit"); bytes = Cryption.AES.Rijndaelcrypt(bytes, txt_pwd.Text); UpdateStatus("Successful!");
                }
                else if (cb_encryption.Text == "Blowfish")
                {
                    Cryption.BlowFish b = new Cryption.BlowFish(txt_pwd.Text);
                    UpdateStatus("Encrypting bytes... Blowfish"); bytes = b.Encrypt_CBC(bytes); UpdateStatus("Successful!");
                }
                else if (cb_encryption.Text == "AES 256 Bit - Blowfish")
                {
                    Cryption.BlowFish b = new Cryption.BlowFish(txt_pwd.Text);
                    UpdateStatus("Encrypting bytes... Blowfish"); bytes = Cryption.AES.Rijndaelcrypt(b.Encrypt_CBC(bytes), txt_pwd.Text); UpdateStatus("Successful!");
                }

                using (ResourceWriter w = new ResourceWriter("res.resources"))
                {
                    w.AddResource("prgrm", bytes);
                    w.AddResource("encryption", cb_encryption.Text);
                    w.AddResource("md5", Cryption.MD5.getMD5(txt_pwd.Text));
                    w.AddResource("name", System.IO.Path.GetFileName(txt_path.Text));
                    w.AddResource("version", version);
                    if (cb_keyfile.Checked == true)
                    {
                        w.AddResource("keyfile", "true");
                    }
                    else
                    {
                        w.AddResource("keyfile", "false");
                    }
                    w.Generate();
                }

                SaveFileDialog sfd2 = new SaveFileDialog();
                sfd2.Filter = "Executables (*.exe)|*.exe";
                if (sfd2.ShowDialog() == DialogResult.OK)
                {
                    string strTempFile = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\ark.ico";

                    using (var fStream = new FileStream(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "ark.ico"), FileMode.OpenOrCreate, FileAccess.Write))
                    {
                        Properties.Resources.ark.Save(fStream);
                    }

                    if (!Compiler.Compile(sfd2.FileName, Properties.Resources.src, strTempFile, "res.resources"))
                    {
                        UpdateStatus("The compiler failed to generate an executable file.");
                        MessageBox.Show(this, "The compiler failed to generate an executable file.", "Compiler Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        UpdateStatus("The compiler generated succesful an executable file.");
                        MessageBox.Show(this, "The compiler generated succesful an executable file.", "Compiler Message!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    onClose();
                    paths.Clear();
                    lb_items.Items.Clear();
                    lb_status.Items.Clear();
                    txt_path.Clear();
                    txt_pwd.Clear();
                    txt_pwd1.Clear();
                }
                
            }
            else
            {
                MessageBox.Show(this, "Passwords do not match!", "Wrong Passwords!", MessageBoxButtons.OK);
                UpdateStatus("Passwords do not match!");
                txt_pwd.Clear(); txt_pwd1.Clear();
            }
        }

        private void UpdateStatus(string input)
        {
            lb_status.Items.Add(input);
            int visibleItems = lb_status.ClientSize.Height / lb_status.ItemHeight;
            lb_status.TopIndex = Math.Max(lb_status.Items.Count - visibleItems + 1, 0);
        }

        private void onClose()
        {
            try
            {
            System.IO.File.Delete("res.resource");
            System.IO.File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\ark.ico");
            System.IO.File.Delete(appdata + @"\data.zip");
            System.IO.Directory.Delete(appdata + @"\sCrypt", true);
            }catch {}
        }

        private void tsb_exit_Click(object sender, EventArgs e)
        {
            onClose();
            Environment.Exit(0);
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            onClose();
            Environment.Exit(0);
        }

        private void cb_keyfile_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_keyfile.Checked == true)
            {
                if (MessageBox.Show(this, "Should a random Key be generated, for use with this Keyfile? Warning you won't know the Key and just the Keyfile will be able to decrypt your data!", "Random Key?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    string pw = PW(60);
                    txt_pwd.Text = pw;
                    txt_pwd1.Text = pw;
                }
            }
        }

        private static string PW(int l)
        {
            string ret = string.Empty;
            System.Text.StringBuilder SB = new System.Text.StringBuilder();
            string Content = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvw!öäüÖÄÜß\"§$%&/()=?*#-";
            Random rnd = new Random();
            for (int i = 0; i < l; i++)
                SB.Append(Content[rnd.Next(Content.Length)]);
            return SB.ToString();
        }

        private void lb_items_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            foreach (string file in files)
            {
                System.IO.File.Copy(file, appdata + @"\sCrypt\" + Path.GetFileName(file));
                paths.Add(appdata + @"\sCrypt" + Path.GetFileName(file));
                lb_items.Items.Add(Path.GetFileName(file));
            }
        }

        private void lb_items_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false) == true)
            {
                e.Effect = DragDropEffects.All;
            }
        }

    }
}
